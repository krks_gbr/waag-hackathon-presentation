


- make sure you have at least node v6.0
    - download: https://nodejs.org/en/download/

- download and install git LFS to be able to add images and videos to the repository
    - either `https://github.com/github/git-lfs/releases/download/v1.4.4/git-lfs-darwin-amd64-1.4.4.tar.gz`
    - or `brew install git-lfs`

- once git lfs is installed, let git know by running `git lfs install` (i know it's confusing, we've already installed it haven't we?)
- add any images or videos to public/files
    - file pattern: <project>_<image_number>.<extension>
- if your filetype is not in .gitattributes, give it to git lfs by `git lfs track <extension>` so `git lfs track .png` for example
- start the server `npm start`
- if you get a warning in the terminal about your file type not being supported, add it to the file types list in `lib/list-files`
