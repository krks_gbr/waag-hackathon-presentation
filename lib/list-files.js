



const fs = require('fs');
const path = require('path');


const fileTypes = {

    image: ['png', 'jpg', 'gif', 'jpeg'].map(ext => `.${ext}`),
    video: ['mp4', 'mov'].map(ext => `.${ext}`)

};

module.exports = function(dirPath){


    const files = fs.readdirSync( dirPath )
        .filter(f => fs.lstatSync( path.join(dirPath, f) ).isDirectory())
        .map(directory => {

                let fullDirPath = path.join(dirPath, directory);
                let captions = JSON.parse(fs.readFileSync(path.join(fullDirPath, 'captions.json')));
                let files =
                    fs.readdirSync(fullDirPath)
                        .filter(f => fileTypes.image.concat(fileTypes.video).indexOf(path.extname(f)) > -1 )
                        .map(f => {
                            let type;
                            let ext = path.extname(f);
                            if(fileTypes.image.indexOf(ext) > -1 ){
                                type = 'image'
                            } else if(fileTypes.video.indexOf(ext) > -1){
                                type = 'video'
                            } else {
                                type = 'unknown';
                                console.log('file type of ', f, ' is not supported and will be excluded');
                            }

                            return {
                                fileName: f,
                                url: path.join('files', directory, f),
                                type: type,
                                captions: captions
                            }
                        });
                return Object.assign(files, captions)
        });

    //return flattened
    return [].concat.apply([], files);
};