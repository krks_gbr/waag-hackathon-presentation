


module.exports = files => {


    return `
    <html>
        <head>
            
            <link rel="stylesheet" href="/css/style.css"/>
            <script>
                window.__files__ = ${JSON.stringify(files)};
            </script>
            
        </head>
        <body>
      
             <div id="gallery"></div>
             <div id="fullscreen">
                 <img class="align-center"/>
                 <div class="caption">
                 </div>
             </div>
             <script src="/js/script.js"></script>

        </body>
    
    </html>
    `

};