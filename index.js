
var express = require('express');
var renderTemplate = require('./lib/render-template');
const listFiles = require('./lib/list-files');
const path = require('path');



var app = express();
app.use(express.static(__dirname + '/public'));


app.set("view engine", "html");
app.set("views", __dirname + "/views");


const filesDir = path.join(__dirname, 'public', 'files');

app.get('/', function (req, res) {
    const files = listFiles(filesDir);
    res.send(renderTemplate(files));
});


var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});
