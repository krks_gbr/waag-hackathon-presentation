
does data have an inherent narrative/perspective or do we give these to data?

is there such a thing as raw data?



- collect a few points to discuss

## Subject of the presentation
- finding narratives in data / giving narratives to data
- how data changes perspective

## Amir
    - input data: webpages
        - different types of content in the webpages
    - narrative
        - filtering unnecessary data through web pages
        - the design of websites impose a narrative on users
            - the way one navigates through the site, what types of information one will find important and how one experiences the information presented is to a large part determined by the set of design decesions that generated the website
        - RE gives power to users override these narratives with ones that they create

## Vera
    -


- collect list of arguments
- collect text, video, image material
thesis-records/


# intro
    - introduce ourselves
    - we talk about how we came up with the title
        - we debate
            perspective vs narrative
            finding vs giving narrative/perspective


# body
    - conversation about the various points collected
    - use image/text/sound/video material collected to
    support arguments, points being made
    - how narrative of perspective is present in our (data driven) work


# conclusion
    - something like "hope you guys found these examples useful etc."