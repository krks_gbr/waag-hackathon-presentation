

const files = window.__files__;

const FileTypes = {
    IMAGE: 'image',
    VIDEO: 'video'
};

const imagesDIR = "/files";

var fullscreen;
var gallery;
var isFullscreen;
var currentIndex = 0;




function loadFullScreenContent() {
    let file = files[currentIndex];

    fullscreen.setAttribute('content-type', file.type);
    let caption = fullscreen.querySelector('.caption');
    caption.innerHTML = `
                 <div>${file.captions.name}</div>
                 <div>${file.captions.title}</div>
                 <div>${file.captions.year}</div>
    `;

    switch (file.type) {
        case FileTypes.IMAGE:
            let img = fullscreen.querySelector('img');
            img.src = file.url;
            break;

        case FileTypes.VIDEO:
            // let video = fullscreen.querySelector('source');
            // video.src = file.url;
            let preVideo = fullscreen.querySelector('video');
            if(preVideo)
              fullscreen.removeChild(preVideo);

            let vidInstance = file.elem.cloneNode(true);
            vidInstance.controls = true;
            fullscreen.insertBefore(vidInstance, caption);
            break;
        default:
    }

}

function updateIndex(idx) {
    currentIndex = idx;
    if (idx > files.length - 1)
        currentIndex = 0;
    else if (idx < 0)
        currentIndex = files.length - 1;
}

function goFullscreen(targetIdx) {
    isFullscreen = true;

    fullscreen.classList.add('on');
    document.body.classList.add('fullscreen');
    updateIndex(targetIdx);
    loadFullScreenContent();
}

function exitFullScreen() {
    fullscreen.classList.remove('on');
    document.body.classList.remove('fullscreen');

    isFullscreen = false;

    let image = fullscreen.querySelector('img');

    if(image)
      image.src = "";

    let video = fullscreen.querySelector('video');

    if(video)
      fullscreen.removeChild(video);
}


function goToSlide(dir) {
    updateIndex(currentIndex + dir);
    console.log(dir, currentIndex);
    loadFullScreenContent();
}


function createFigure(file,index) {
    let thumb = document.createElement('figure');
    thumb.className = "thumb";
    thumb.style.backgroundImage = `url('${file.url}')`;
    thumb.onclick = (e) => goFullscreen(index);

    return thumb;
}

function createVideoThumb(file,index) {
    let videoThumb = document.createElement('video');
    let source = document.createElement('source');

    videoThumb.classList.add('thumb');
    source.src = file.url;
    videoThumb.onclick = (e) => goFullscreen(index);

    videoThumb.appendChild(source);

    file['elem'] = videoThumb;

    return videoThumb;
}

function addThumb(file, index) {

    switch (file.type) {
        case FileTypes.IMAGE:
            gallery.appendChild(createFigure(file,index));
            break;

        case FileTypes.VIDEO:
            gallery.appendChild(createVideoThumb(file,index));
            break;
        default:
    }

}




document.onkeydown = (e)=> {
    e = e || window.event;

    if (!isFullscreen) return;

    switch (e.keyCode) {
        case 37:
        case 40:
            goToSlide(-1);
            break;
        case 38:
        case 39:
            goToSlide(+1);
            break;
        case 27:
            exitFullScreen();
            break;
        default:
            break;
    }

};


window.onload = e => {
    fullscreen = document.getElementById('fullscreen');
    gallery = document.getElementById('gallery');
    files.forEach(addThumb);
};
