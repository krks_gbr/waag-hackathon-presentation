
/*
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 DO NOT RUN THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */


let fs = require('fs');
let path = require('path');
let captionTemplate = require('./lib/caption-template');


const projectsRoot = path.join(__dirname, 'public', 'files');


fs.readdirSync( projectsRoot )
    .map(f => path.join(projectsRoot, f))
    .filter(f => fs.lstatSync(f).isDirectory())
    .forEach(directory => {
        fs.writeFileSync(path.join(directory, 'captions.json'), JSON.stringify(captionTemplate, null, 4));
    });

